import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import * as tf from '@tensorflow/tfjs';
import '@tensorflow/tfjs-react-native';
import { bundleResourceIO } from '@tensorflow/tfjs-react-native';
import { DeviceMotion } from 'expo-sensors';


export default function App() {

  const [ready, setReady] = useState(false);
  const [model, setModel] = useState(undefined);

  const [predictions, setPredictions] = useState([]);


  async function initTensorFlow() {
    await tf.ready();

    const ml = await tf.loadLayersModel(bundleResourceIO(
      require('./models/model.json'), 
      require('./models/weights.bin')));

    console.log(ml);

    setModel(ml);
  }

  async function predict(data) {
    console.log(data);
    if(!model) return;
    let test = [];
    test.push(data);
    setPredictions(model.predict(tf.tensor2d(test)).arraySync());
  }

  async function rec() {
    let mesures = [];
    let subscription;
    subscription = DeviceMotion.addListener(data => {
      if(mesures.length === 128*3) {
        predict(mesures);
        subscription.remove();
      } else {
        mesures.push(data.acceleration.x);
        mesures.push(data.acceleration.y);
        mesures.push(data.acceleration.z);
      }
    });
  }

  useEffect(() => {
    DeviceMotion.setUpdateInterval(10);
    initTensorFlow();
  }, []);

  return (
    <View style={styles.container}>
      {model && <Text>Ready</Text>}
      <Button onPress={rec} style={styles.button} title="REC"/>
      {predictions.length != 0 && <Text>{predictions[0][0]}</Text>}
      {predictions.length != 0 && <Text>{predictions[0][1]}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    color: 'black'
  }
});
